package gra;

public class Test {

	public static void main(String[] args) {
		test1(); //Poruszanie się postaci z czekaniem az pole sie zwolni.
		//test2(); //Wykrywanie deadlocka kiedy powstaje cykl 4 postaci.
		//test3(); //Ogolne poruszanie sie. Jedna postac jest usuwana z planszy. 
		//test4(); //Proba wyjscia za mape.
		//test5(); //Proba postawienia sie postaci w rogu mapy, tak, ze sie nie miesci
		//test6(); //Proba usuniecia postaci ktorej nie ma na mapie
		//test7(); //Demonstracja metody sprawdz na zajetym i wolnym polu
		//test8(); //Czekanie na pola w celu postawienia sie na planszy
	}
	
	
	/*
	 * Poruszanie się postaci z czekaniem az pole sie zwolni.
	 */
	public static void test1() {
		System.out.println("-------------------------------test1-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 0 , 0);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
				
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 0 , 2);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t3 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 2 , 0);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t4 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 2, 2);
					plansza.drawState();
					Thread.sleep(5000);
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		try {
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t1.join();
			t2.join();
			t3.join();
			t4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Wykrywanie deadlocka kiedy powstaje cykl 4 postaci.
	 */
	public static void test2() {
		System.out.println("-------------------------------test2-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 0 , 0);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();	
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 0 , 2);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t3 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 2 , 0);
					plansza.drawState();
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.PRAWO);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t4 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					plansza.postaw(p, 2, 2);
					plansza.drawState();
					Thread.sleep(5000);
					plansza.przesuń(p, Kierunek.GÓRA);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.GÓRA);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		try {
			t1.start();
			t2.start();
			t3.start();
			t4.start();
			t1.join();
			t2.join();
			t3.join();
			t4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Ogolne poruszanie sie. Jedna postac jest usuwana z planszy. 
	 */
	public static void test3() {
		System.out.println("-------------------------------test3-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(3, 4);
				try {
					plansza.postaw(p, 1 , 1);
					plansza.drawState();
					Thread.sleep(1000);
					
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
					
					plansza.usuń(p);
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(1, 1);
				try {
					plansza.postaw(p, 8 , 8);
					Thread.sleep(1000);
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					plansza.przesuń(p, Kierunek.LEWO);
					
					Thread.sleep(1000);
					plansza.drawState();

				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
			}
		});
		
		try {
			t1.start();
			t2.start();
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Proba wyjscia za mape
	 */
	public static void test4() {
		System.out.println("-------------------------------test4-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(3, 4);
				try {
					plansza.postaw(p, 0 , 0);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					
					plansza.usuń(p);
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		try {
			t1.start();
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Proba postawienia sie postaci w rogu mapy, tak, ze sie nie miesci
	 */
	public static void test5() {
		System.out.println("-------------------------------test5-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(3, 4);
				try {
					plansza.postaw(p, 8 , 8);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					
					plansza.usuń(p);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		try {
			t1.start();
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Proba usuniecia postaci ktorej nie ma na mapie
	 */
	public static void test6() {
		System.out.println("-------------------------------test6-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(3, 4);
				try {
					plansza.postaw(p, 5 , 5);
					plansza.drawState();
					
					plansza.przesuń(p, Kierunek.LEWO);
					plansza.drawState();
					
					plansza.usuń(p);
					plansza.drawState();
					
					plansza.usuń(p);
					plansza.drawState();
					
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		try {
			t1.start();
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	/*
	 * Demonstracja metody sprawdz na zajetym i wolnym polu
	 */
	public static void test7() {
		System.out.println("-------------------------------test7-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(3, 4);
				try {
					plansza.postaw(p, 5 , 5);
					plansza.drawState();
					
					plansza.sprawdź(5, 5, new Akcja() {
						public void wykonaj(Postać postać) {
							System.out.println("sprawdz - zajete");
						}}, new Runnable(){
						public void run() {
							System.out.println("sprawdz - wolne");
						}});
					
					plansza.sprawdź(5, 4, new Akcja() {
						public void wykonaj(Postać postać) {
							System.out.println("sprawdz - zajete");
						}}, new Runnable(){
						public void run() {
							System.out.println("sprawdz - wolne");
						}});
					
					
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} 
			}
		});
		
		try {
			t1.start();
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Czekanie na pola w celu postawienia sie na planszy
	 */
	public static void test8() {
		System.out.println("-------------------------------test8-----------------------------");
		final MojaPlansza plansza = new MojaPlansza(10, 10);
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(4, 3);
				try {
					plansza.postaw(p, 5 , 5);
					plansza.drawState();
					Thread.sleep(2000);
					
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
					
					Thread.sleep(2000);
					plansza.przesuń(p, Kierunek.DÓŁ);
					plansza.drawState();
				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} catch (DeadlockException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				TestowaPostać p = new TestowaPostać(2, 2);
				try {
					Thread.sleep(500);
					plansza.postaw(p, 5 , 5);
					plansza.drawState();

				} catch (InterruptedException e) {
					e.printStackTrace();
					Thread.currentThread().interrupt();
				} 
			}
		});
		
		try {
			t1.start();
			t2.start();
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
