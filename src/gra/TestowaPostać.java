package gra;

public class TestowaPostać implements Postać{
	private final int szer;
	private final int wys;
	
	public TestowaPostać(int szer, int wys) {
		this.szer = szer;
		this.wys = wys;
	}
	
	public int dajWysokość() {
		return wys;
	}

	public int dajSzerokość() {
		return szer;
	}

}
