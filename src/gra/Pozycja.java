package gra;

public class Pozycja {
	private int wiersz;
	private int kolumna;
	
	public Pozycja(int wiersz, int kolumna) {
		this.wiersz = wiersz;
		this.kolumna = kolumna;
	}
	
	public int dajWiersz() {
		return wiersz;
	}
	
	public int dajKolumnę() {
		return kolumna;
	}
}