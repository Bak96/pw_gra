package gra;

import java.util.concurrent.locks.Condition;

/*
 * Klasa opakowująca Postać.
 * Trzyma dodatkowe informacje, które są wymagane do trzymania Postaci na planszy.
 */

public class MojaPostać {
	private Postać postać;
	private int wiersz;
	private int kolumna;
	
	private boolean będęRuszać = false;
	private Kierunek kierunek = null;
	private Condition condition; //na tym condition czekaja postacie chcace wejsc na pola, ktore zajmuje.
	
	public MojaPostać(Postać postać, int wiersz, int kolumna, Condition condition) {
		this.postać = postać;
		this.wiersz = wiersz;
		this.kolumna = kolumna;
		this.condition = condition;
	}
	
	public Postać dajPostać() {
		return postać;
	}
	
	public int dajWiersz() {
		return wiersz;
	}
	
	public int dajKolumnę() {
		return kolumna;
	}
	
	public void zmieńPozycję(int wiersz, int kolumna) {
		this.wiersz = wiersz;
		this.kolumna = kolumna;
	}
	
	public void czekajNaJejRuch() throws InterruptedException {
		condition.await();
	}
	
	public void zwolnijPola() {
		condition.signalAll();
	}
	
	public boolean czyChceSięRuszyć() {
		return będęRuszać;
	}
	
	public void chcęSięRuszyć(boolean chcę) {
		będęRuszać = chcę;
	}
	
	public void ustawKierunek(Kierunek kierunek) {
		this.kierunek = kierunek;
	}
	
	public Kierunek dajKierunek() {
		return kierunek;
	}
}
