package gra;


import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MojaPlansza implements Plansza {
	private final int WYSOKOŚĆ_PLANSZY;
	private final int SZEROKOŚĆ_PLANSZY;
	private final Lock lock = new ReentrantLock(); // zapewnia wylacznosc nad dostepem do listyPionkow
	private final List<MojaPostać> listaPionków = new LinkedList<MojaPostać>();
	
	public MojaPlansza(int wysokość, int szerokość) {
		if (wysokość <= 0 || szerokość <= 0) {
			throw new IllegalArgumentException();
		}
		
		this.WYSOKOŚĆ_PLANSZY = wysokość;
		this.SZEROKOŚĆ_PLANSZY = szerokość;
	}
	
	/*
	 * Umieszcza na planszy postać, zaczynając od wiersza i kolumny.
	 * Jeśli umieszczenie postaci na planszy nie jest możliwe od razu, bo część potrzebnych pól jest zajętych,
	 * wątek wykonujący metodę jest zawieszany do chwili, gdy wszystkie pola, które postać ma zająć, będą wolne.
	 */
	public void postaw(Postać postać, int wiersz, int kolumna)
			throws InterruptedException {
		
		lock.lock();
		try {
			if (jestNaPlanszy(postać) || !mieściSięNaPlanszy(postać, wiersz, kolumna)) {
				throw new IllegalArgumentException();
			}
			
			boolean sprawdzajKolizje = true;			
			while(sprawdzajKolizje) {
				MojaPostać postaćKolidująca = kolizja(postać, wiersz, kolumna);
						
				if (postaćKolidująca != null) {
					postaćKolidująca.czekajNaJejRuch();
				}
				else {
					sprawdzajKolizje = false;
				}
			}
	
			MojaPostać nowaPostaćNaMapie = new MojaPostać(postać, wiersz, kolumna, lock.newCondition());
			listaPionków.add(nowaPostaćNaMapie);
		}
		finally {
			lock.unlock();
		}
	}

	
	/*
	 * Przesuwa na planszy postać o jedno pole w podanym kierunku.
	 * Jeżeli nie jest to możliwe od razu, bo któreś z wymaganych pól jest zajęte,
	 * wątek wykonujący metodę jest zawieszany do chwili, gdy przesunięcie postaci będzie możliwe.
	 */
	public void przesuń(Postać postać, Kierunek kierunek)
			throws InterruptedException, DeadlockException {
		lock.lock();
		
		try {
			MojaPostać mojaPostać = dajPostaćZPlanszy(postać);
			if(mojaPostać == null) {
				throw new IllegalArgumentException();
			}
			
			Pozycja nowaPozycja = obliczNowąPozycję(mojaPostać, kierunek);
			if (!mieściSięNaPlanszy(postać, nowaPozycja.dajWiersz(), nowaPozycja.dajKolumnę())) {
				throw new IllegalArgumentException();
			}
			
			mojaPostać.chcęSięRuszyć(true);
			mojaPostać.ustawKierunek(kierunek);
			
			if (deadlock(mojaPostać, nowaPozycja.dajWiersz(), nowaPozycja.dajKolumnę())) {
				mojaPostać.chcęSięRuszyć(false);
				throw new DeadlockException();
			}
			
			boolean sprawdzajKolizje = true;
			while (sprawdzajKolizje) {
				MojaPostać postaćKolidująca = kolizja(postać, nowaPozycja.dajWiersz(), nowaPozycja.dajKolumnę());
				
				if (postaćKolidująca != null) {
					postaćKolidująca.czekajNaJejRuch();
				}
				else {
					sprawdzajKolizje = false;
				}
			}
			
			mojaPostać.zwolnijPola();
			mojaPostać.zmieńPozycję(nowaPozycja.dajWiersz(), nowaPozycja.dajKolumnę());
			mojaPostać.chcęSięRuszyć(false);
		}
		finally {
			lock.unlock();
		}
	}
	
	/*
	 * Usuwa postać z planszy.
	 */
	public void usuń(Postać postać) {
		lock.lock();
		
		try {
			Iterator<MojaPostać> iter = listaPionków.iterator();
			while (iter.hasNext()) {
				MojaPostać mp = iter.next();
				
				if (mp.dajPostać() == postać) {
					mp.zwolnijPola();
					iter.remove();
					return;
				}
			}
			
			//nie znalazlem postaci na liscie
			throw new IllegalArgumentException();
			
		}
		finally {
			lock.unlock();
		}
	}

	/*
	 * Sprawdza, czy pole w wierszu i kolumnie jest zajęte.
	 * Jeśli tak, to na znajdującej się na nim postaci wykonuje akcję jeśliZajęte,
	 * w przeciwnym przypadku wykonuje  jeśliWolne.
	 */
	public void sprawdź(int wiersz, int kolumna, Akcja jeśliZajęte,
			Runnable jeśliWolne) {
		
		lock.lock();
		try {
			if(!mieściSięNaPlanszy(wiersz, kolumna)) {
				throw new IllegalArgumentException();
			}
			
			MojaPostać mojaPostać = dajPostaćZPlanszy(wiersz, kolumna);
			if (mojaPostać != null) {
				jeśliZajęte.wykonaj(mojaPostać.dajPostać());
			}
			else {
				jeśliWolne.run();
			}
		}
		finally {
			lock.unlock();
		}
	}
	
	/*
	 * Sprawdza czy postać mieści się na planszy gdyby stała na polu wiersz, kolumna
	 */
	private boolean mieściSięNaPlanszy(Postać postać, int wiersz, int kolumna) {
		if (wiersz < 0 || kolumna < 0 ||
				wiersz + postać.dajWysokość() > WYSOKOŚĆ_PLANSZY ||
				kolumna + postać.dajSzerokość() > SZEROKOŚĆ_PLANSZY) {
			return false;
		}
		return true;
	}
	
	/*
	 * Sprawdza czy punkt mieści się w obrębie planszy
	 */
	private boolean mieściSięNaPlanszy(int wiersz, int kolumna) {
		if (wiersz < 0 || kolumna < 0 ||
				wiersz >= WYSOKOŚĆ_PLANSZY ||
				kolumna >= SZEROKOŚĆ_PLANSZY) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * Sprawdza czy postać jest umieszczona na planszy.
	 */
	private boolean jestNaPlanszy(Postać postać) {
		Iterator<MojaPostać> iter = listaPionków.iterator();
		while (iter.hasNext()) {
			MojaPostać p = iter.next();
			if (p.dajPostać() == postać) {
				return true;
			}
		}
		
		return false;
	}
	
	/*
	 * Zwraca pierwsza postać z ktora koliduje, lub null jesli nie koliduje z zadna postacia.
	 */
	private MojaPostać kolizja(Postać postać, int wiersz, int kolumna) {
		Iterator<MojaPostać> iter = listaPionków.iterator();
		
		while(iter.hasNext()) {
			MojaPostać mp = iter.next();
			Postać p = mp.dajPostać();
			
			if (p == postać) {
				continue;
			}
			
			if (mp.dajKolumnę() < kolumna + postać.dajSzerokość() &&
					mp.dajKolumnę() + p.dajSzerokość() > kolumna &&
					mp.dajWiersz() < wiersz + postać.dajWysokość() &&
					mp.dajWiersz() + p.dajWysokość() > wiersz) {
				return mp;
			}
		}
		
		return null;
	}
	
	/*
	 * Pobiera szczegółowe informacje o postaci znajdującej się na planszy.
	 */
	private MojaPostać dajPostaćZPlanszy(Postać postać) {
		Iterator<MojaPostać> iter = listaPionków.iterator();
		while (iter.hasNext()) {
			MojaPostać mp = iter.next();
			if (mp.dajPostać() == postać) {
				return mp;
			}
		}
		
		return null;
	}
	
	/*
	 * Pobiera szczegółowe informacje o postaci znajdującej się w polu wiersz, kolumna
	 * lub zwraca null jesli nie ma żadnej postaci stojącej na tym polu.
	 */
	private MojaPostać dajPostaćZPlanszy(int wiersz, int kolumna) {
		Iterator<MojaPostać> iter = listaPionków.iterator();
		while(iter.hasNext()) {
			MojaPostać mp = iter.next();
			Postać p = mp.dajPostać();
			if (wiersz >= mp.dajWiersz() && wiersz < mp.dajWiersz() + p.dajWysokość() &&
					kolumna >= mp.dajKolumnę() && kolumna < mp.dajKolumnę() + p.dajSzerokość()) {
				return mp;
			}
		}
		return null;
	}
	
	/*
	 * Wylicza nowe współrzędne dla postaci, która chce się przesunąć w podanym kierunku
	 */
	private Pozycja obliczNowąPozycję(MojaPostać mojaPostać, Kierunek kierunek) {
		int wiersz = mojaPostać.dajWiersz();
		int kolumna = mojaPostać.dajKolumnę();
		
		switch(kierunek) {
		case GÓRA:
			wiersz--;
			break;
		case DÓŁ:
			wiersz++;
			break;
		case LEWO:
			kolumna--;
			break;
		case PRAWO:
			kolumna++;
			break;
		}
		
		return new Pozycja(wiersz, kolumna);
	}
	
	/*
	 * Zwraca zbiór postaci z którymi dana postać ma kolizję jeśli stałaby na polu wiersz, kolumna.
	 */
	private Set<MojaPostać> dajWszystkieKolizje(Postać postać, int wiersz, int kolumna) {
		Set<MojaPostać> kolidującePostacie = new HashSet<MojaPostać>();
		Iterator<MojaPostać> iter = listaPionków.iterator();
		
		while(iter.hasNext()) {
			MojaPostać mp = iter.next();
			Postać p = mp.dajPostać();
			
			if (p == postać) {
				continue;
			}
			
			if (mp.dajKolumnę() < kolumna + postać.dajSzerokość() &&
					mp.dajKolumnę() + p.dajSzerokość() > kolumna &&
					mp.dajWiersz() < wiersz + postać.dajWysokość() &&
					mp.dajWiersz() + p.dajWysokość() > wiersz) {
				kolidującePostacie.add(mp);
			}
		}
		
		return kolidującePostacie;
	}
	
	/*
	 * Wykrywa czy postać powoduje deadlock, jeśli chce stanąć w polu nowyWiersz, nowaKolumna
	 */
	private boolean deadlock(MojaPostać mojaPostać, int nowyWiersz, int nowaKolumna) {
		boolean jestDeadlock = false;
		Set<MojaPostać> kolizje = dajWszystkieKolizje(mojaPostać.dajPostać(), nowyWiersz, nowaKolumna);
		
		while (!kolizje.isEmpty() && !jestDeadlock) {
			if(kolizje.contains(mojaPostać)) {
				jestDeadlock = true;
			}
			
			Set<MojaPostać> kolizjeSąsiadów = new HashSet<MojaPostać>();
			for (MojaPostać sąsiad: kolizje) {
				if (!sąsiad.czyChceSięRuszyć()) {
					continue;
				}
				
				Pozycja nowaPozycja = obliczNowąPozycję(sąsiad, sąsiad.dajKierunek());
				Set<MojaPostać> tmpSet = dajWszystkieKolizje(sąsiad.dajPostać(), nowaPozycja.dajWiersz(), nowaPozycja.dajKolumnę());
				kolizjeSąsiadów.addAll(tmpSet);
			}
			kolizje = kolizjeSąsiadów;
		}
		
		return jestDeadlock;
	}
	
	/*
	 * Wypisuje stan planszy. Funkcja służąca do debugu
	 */
	public void drawState() {
		lock.lock();
		char[][] mapa = new char[WYSOKOŚĆ_PLANSZY][SZEROKOŚĆ_PLANSZY];
		
		for (int wiersz = 0; wiersz < WYSOKOŚĆ_PLANSZY; wiersz++) {
			for (int kolumna = 0; kolumna < SZEROKOŚĆ_PLANSZY; kolumna++) {
				mapa[wiersz][kolumna] = '*';
			}
		}
		
		int id = 0;
		for (MojaPostać mp : listaPionków) {
			Postać p = mp.dajPostać();
			
			for (int wiersz = mp.dajWiersz(); wiersz < mp.dajWiersz() + p.dajWysokość(); wiersz++) {
				for (int kolumna = mp.dajKolumnę(); kolumna < mp.dajKolumnę() + p.dajSzerokość(); kolumna++) {
					mapa[wiersz][kolumna] = (char)(id+48);
				}
			}
			id++;
		}
		
		for (int wiersz = 0; wiersz < WYSOKOŚĆ_PLANSZY; wiersz++) {
			for (int kolumna = 0; kolumna < SZEROKOŚĆ_PLANSZY; kolumna++) {
				System.out.print(mapa[wiersz][kolumna]);
				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println("------------------------------------------------------^" + Thread.currentThread().getName() );
		lock.unlock();
	}
}

